# Scrap Shows


[![CircleCI](https://circleci.com/gh/icaromh/scrap-shows.svg?style=svg)](https://circleci.com/gh/icaromh/scrap-shows)


This project scrap all the shows from [http://brseries.com](http://brseries.com), then transform to a `PopcornTime Show Schema`, merging the show description, duration, images, episodes images to mp4 video file found at BrSeries.


### Setup

To run locally this project you will need:
- Docker (or mongodb)
- python > 3.6

First of all create an virtualenv
```
python3 -m venv .venv
source .venv/bin/activate
```

Install the dependencies:
```
make setup
```

Configure your environment variables
```
cp .env.default .env
```
You will need update the `MONGODB_URI` variable to your own mongodb host or mantain this one that works fine within docker container.

Ensure that docker is running on your machine and start the project

```
make run-local
```

### Requirements
- Docker
- Python 3.6.2

### TODO

- Search show/episode images from TVDB if TMDB fails
- Split providers in workers
