FROM python:3.6.1

ENV app /app/scrapper
ENV PYTHONUNBUFFERED true

RUN mkdir -p ${app}
WORKDIR ${app}
VOLUME ${app}

COPY requirements* /tmp/
RUN pip install -r /tmp/requirements.txt

COPY . ${app}

CMD python -m scrapper
