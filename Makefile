setup:
	pip install -r requirements_dev.txt

run:
	python -m scrapper

docker-build:
	@docker build -t dollyflix/scrapper .

docker-clean:
	-docker kill dollyflix_scrapper
	-docker rm dollyflix_scrapper
	-docker kill dollyflix_mongo
	-docker rm dollyflix_mongo

docker-run: docker-clean docker-build
	docker run --rm -d -p 27017:27017 --name dollyflix_mongo mongo
	docker run -t -v `pwd`:/app/scrapper \
		--link dollyflix_mongo:mongo \
		--env MONGODB_URI=mongodb://mongo:27017/dollyflix \
		--name dollyflix_scrapper -i dollyflix/scrapper

lint:
	pycodestyle ./scrapper

test:
	py.test -q

deploy:
	git push heroku master
