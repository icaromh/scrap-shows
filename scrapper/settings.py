import os
import dotenv

dotenv.load_dotenv(
    dotenv.find_dotenv()
)


class Settings:

    BRSERIES_URL = 'http://www.brseries.com/category/series/'

    DEBUG = int(os.getenv('DEBUG', 0))
    LOG_LEVEL = os.getenv('LOG_LEVEL', 'INFO')

    TRAKT_CLIENT_ID = os.getenv('TRAKT_CLIENT_ID', '70c43f8f4c0de74a33ac1e66b6067f11d14ad13e33cd4ebd08860ba8be014907')  # NOQA
    TMDB_API_KEY = os.getenv('TMDB_API_KEY', 'a5bdfcf07f6afbd5ec818e8a9bbd2a5b')  # NOQA

    MONGODB_URI = os.getenv('MONGODB_URI', 'mongodb://localhost:27017/brseries')  # NOQA
    MONGODB_DB = os.getenv('MONGODB_URI', 'mongodb://localhost:27017/brseries').split('/')[-1]  # NOQA

    # Slugs mapped for trakt slugs
    SHOWS_MAP = {
        'mike-and-molly': 'mike-molly',
        'the-mick': 'the-mick-2017',
        'the-borgias': 'the-borgias-2011',
        'rizzoli-and-isles': 'rizzoli-isles',
        'swat': 's-w-a-t-2017',
        'greys-anatomy': 'grey-s-anatomy',
        'twisted': 'twisted-2013',
        'inhumans': 'marvel-s-inhumans',
        'the-black-list': 'the-blacklist',
        'timeless': 'timeless-2016',
        'startup': 'startup-2016',
        'dcs-legends-of-tomorrow': 'dc-s-legends-of-tomorrow',
        'todo-mundo-odeia-o-chris': 'everybody-hates-chris',
        'stark-trek-discovery': 'star-trek-discovery',
        'the-shannara-chronicle': 'the-shannara-chronicles',
        'kevin-can-wait': 'kevin-can-wait-2016',
        'inti-the-badlands': 'into-the-badlands',
        'chicago-pd': 'chicago-p-d',
        'constantantine': 'constantine',
        'beuaty-and-the-beast': 'beauty-and-the-beast-2012',
        'law-order-svu': 'law-order-special-victims-unit',
        'the-mayor': 'the-mayor-2017',
        'agent-carter': 'marvel-s-agent-carter',
        'ravens-home': 'raven-s-home',
        'better-things': 'better-things-2016',
        'eu-e-a-patroa-as-criancas': 'my-wife-and-kids',
        'two-and-a-half-man-dois-homens-e-meio': 'two-and-a-half-men',
        'arquivo-x-the-x-files': 'the-x-files',
        'agent-us': 'free-agents-us',
        'salvation': 'salvation-2017',
        'gilmore-girls-year-in-the-life': 'gilmore-girls-a-year-in-the-life',
        'time-after-time-us': 'time-after-time-2017',
        'punho-de-ferro': 'marvel-s-iron-fist',
        'demolidor-daredevil': 'marvel-s-daredevil',
        'luke-cage': 'marvel-s-luke-cage',
        'jessica-jones': 'marvel-s-jessica-jones',
        'pustina-wasteland': 'pustina',
        'the-handmaids-tale': 'the-handmaid-s-tale',
        'csi-new-york': 'csi-ny',
        'csi-investigacao-criminal': 'csi-crime-scene-investigation',
        'agents-of-s-h-i-l-d': 'marvel-s-agents-of-s-h-i-e-l-d',
        'dr-house': 'house',
        'grace-and-frakie': 'grace-and-frankie',
        'the-client-list-agenda-proibida': 'the-client-list',
        'ransom': 'ransom-2017',
        'dead-of-summer': 'dead-of-summer-2016',
        'atate-of-affairs': 'state-of-affairs',
        'the-punisher-o-justiceiro': 'marvel-s-the-punisher',
        'disjointed': 'disjointed-2017'
    }
