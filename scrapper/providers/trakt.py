from trakt import core
from datetime import datetime
from trakt.tv import TVShow, TVEpisode
from trakt.errors import NotFoundException
from time import sleep

from scrapper.components.log import Logger
from scrapper.settings import Settings
from scrapper.providers.tmdb import TMDB


class Trakt:

    def __init__(self):
        self.tmdb = TMDB()
        core.CLIENT_ID = Settings.TRAKT_CLIENT_ID
        self.log = Logger.get('trakt_helper')

    @staticmethod
    def sanatize_slug(url):
        " Clean the url and get slug "
        if not url:
            return ''

        if url[-1] == '/':
            url = url[:-1]

        slug = url.split('/')[-1]

        slug = slug.replace('assistir-', '')
        slug = slug.replace('serie-', '')
        slug = slug.replace('-online', '')

        return Settings.SHOWS_MAP.get(slug, slug)

    def get_trakt_info(self, slug):
        """ Retrieve from Trakt all the information needed to Show schema """
        self.log.info('Getting Trakt info for %s' % slug)

        try:
            show = TVShow(slug=slug)
            return {
                'title': show.title,
                'year': show.year,
                'slug': show.slug,
                'imdb_id': show.ids['ids']['imdb'],
                'tvdb_id': show.ids['ids']['tvdb'],
                'tmdb_id': show.ids['ids']['tmdb'],
                'synopsis': show.overview,
                'runtime': show.runtime,
                'country': show.country,
                'network': show.network,
                'status': show.status,
                'num_seasons': 0,
                'last_updated': datetime.utcnow(),
                'latest_episode': 0,
                'images': None,
                'genres': show.genres if len(show.genres) else ['unknown'],
                'episodes': []
            }
        except NotFoundException as e:
            self.log.warning('%s Not Found in Trakt: %s' % (slug, str(e)))
            return None
        except Exception as e:
            self.log.warning('Wrong slug show (%s): %s' % (slug, str(e)))
            return None

    def get_episode_info(self, slug, season, number):
        """ Get the episode description """
        self.log.info(
            'Getting Trakt info for %s S%sE%s' % (slug, season, number))

        try:
            episode = TVEpisode(slug, season, number)
            return {
                'description': episode.get_description(),
                'title': episode.title
            }
        except NotFoundException as e:
            self.log.warning('Err Retrieving episode info for %s S%sE%s: %s' % (slug, season, number, str(e)))  # NOQA
            return {}
        except Exception as e:
            self.log.warning('Err Retrieving episode info for %s S%sE%s: %s' % (slug, season, number, str(e)))  # NOQA
            return {}

    def get_episodes(self, show, episodes_list):
        episodes = []
        slug = show.get('slug')
        tmdb_id = show.get('tmdb_id')

        for episode in episodes_list:
            season = episode.get('season')
            number = episode.get('number')

            episode['image'] = self.tmdb.get_episode_image(tmdb_id, season, number)  # NOQA

            episode_data = self.get_episode_info(slug, season, number)
            episode['description'] = episode_data.get('description', '')
            episode['title'] = episode_data.get('title', '')

            episodes.append(episode)

            # Sleep .5 second for tmdb do now deny our next request
            # @TODO search episodes images in another worker
            sleep(0.5)

        return episodes
