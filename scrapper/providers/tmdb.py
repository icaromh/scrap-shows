import requests
import tmdbsimple as tmdb

from scrapper.settings import Settings
from scrapper.components.log import Logger


class TMDB:

    def __init__(self):
        tmdb.API_KEY = Settings.TMDB_API_KEY
        self.tmdb = tmdb
        self.log = Logger.get('TMDB')

    def get_image_url(self, path, size='w500'):
        """
            Get the image url from tmdb.org
            @path abczzgIi5RdJuH03XVFHhIp2o8L.jpg
            @size of the image
            @return String full image url
        """
        return 'https://image.tmdb.org/t/p/{}{}'.format(size, path)

    def filter_image(self, images):
        if images is not None:
            filtered_images = filter(
                lambda x: x['iso_639_1'] == 'en' or x['iso_639_1'] is None,
                images
            )
            images_list = list(filtered_images)
            return images_list[0] if len(images_list) > 0 else {}

        return {}

    def get_show_image(self, tmdb_id):
        images = {
          "banner": "",  # @TODO add placholder
          "fanart": "",
          "poster": ""
        }

        try:
            show = self.tmdb.TV(tmdb_id).images()
        except requests.exceptions.HTTPError as e:
            # @TODO add TVDB search for images if fail in TMDB
            self.log.warning('Err in get_show_image: %s' % str(e))
            return images

        poster = self.filter_image(show.get('posters'))
        backdrops = self.filter_image(show.get('backdrops'))

        images['poster'] = self.get_image_url(poster.get('file_path', ''))
        images['fanart'] = self.get_image_url(backdrops.get('file_path', ''))
        images['banner'] = self.get_image_url(poster.get('file_path', ''))

        return images

    def get_episode_image(self, tmdb_id, season, number):
        try:
            episode = self.tmdb.TV_Episodes(tmdb_id, season, number)
            stills = episode.images().get('stills', [])
        except requests.exceptions.HTTPError as e:
            self.log.warning(
                'err to get episode %s %s %s: %s' % (
                    tmdb_id, season, number, str(e)))
            stills = []

        if len(stills) > 0:
            return self.get_image_url(stills[0].get('file_path'))

        return ''
