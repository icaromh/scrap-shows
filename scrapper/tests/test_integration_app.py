import validators
from scrapper.app import Scrapper
scrapper = Scrapper()


class TestIntegrationScrapper():
    """
        Integration tests for Scrapper
        Ensure that scrapper know how to get pagination, show title and slug
    """

    def test_get_pages_total_pages(self):
        " Get total pages (today has 15 pages)"

        total_pages = scrapper.get_pages_total_pages()
        assert total_pages > 1

    def test_get_show_data(self):
        " Get all episodes from show URL "

        show_url = 'http://www.brseries.com/assistir-lie-to-me/'
        episodes = scrapper.get_episodes(show_url)

        expected_episodes_len = 48

        assert len(episodes) == expected_episodes_len

    def test_get_shows_from_page(self):
        " Should discover a list of URLS to shows from some page "
        SHOWS_PER_PAGE = 20

        shows = scrapper.get_shows_from_page()

        assert len(shows) == SHOWS_PER_PAGE
        for show in shows:
            assert validators.url(show) is True
