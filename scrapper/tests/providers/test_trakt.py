import validators
from scrapper.providers.trakt import Trakt


class TestTraktProvider():
    """
        Unit tests for Trakt Provider
    """

    def test_empty_slug_sanatize_slug(self):
        slug = Trakt.sanatize_slug('')
        assert slug == ''

    def test_slug_sanatize_slug(self):
        SHOWS = {
            '': '',
            'http://www.brseries.com/assistir-the-flash/': 'the-flash',
            'http://www.brseries.com/assistir-the-flash': 'the-flash',
            'http://www.brseries.com/assistir-serie-arrow/': 'arrow',
            'http://www.brseries.com/csi-investigacao-criminal': 'csi-crime-scene-investigation',  # NOQA: E501
            'http://www.brseries.com/csi-investigacao-criminal-online/': 'csi-crime-scene-investigation',  # NOQA: E501
        }

        for i, url in enumerate(list(SHOWS), start=0):
            expected = SHOWS[url]
            slug = Trakt.sanatize_slug(url)
            assert slug == expected

    def test_get_trakt_info_success(self):
        " Should return a show dict "
        pass

    def test_get_trakt_info_exception_not_found(self):
        " Should return None "
        pass
