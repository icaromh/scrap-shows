from pymongo import MongoClient
from scrapper.settings import Settings


class Mongo:
    """ Client to save shows at the DB """

    def __init__(self):
        self.client = MongoClient(Settings.MONGODB_URI)
        self.db = self.client[Settings.MONGODB_DB]
        self.shows_collection = self.db['shows']

    def find_one(self, slug):
        """ Find one show by slug """
        query = {'slug': slug}
        return self.shows_collection.find_one(query)

    def save_show(self, show):
        """
            Save the show in DB
            expected a show object
            @return the ObjectId
        """
        query = {'slug': show.get('slug')}
        return self.shows_collection.update_one(
            query,
            {
                '$set': show,
            },
            upsert=True
        )
