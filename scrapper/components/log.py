import sys
import logging

from scrapper.settings import Settings


class Logger:

    @classmethod
    def get(cls, name='default'):

        logging.basicConfig(stream=sys.stdout, level=Settings.LOG_LEVEL)
        return logging.getLogger("series:%s" % name)
