import requests
from pyquery import PyQuery as pq
import newrelic.agent


from scrapper.settings import Settings
from scrapper.providers.tmdb import TMDB
from scrapper.providers.trakt import Trakt
from scrapper.components.log import Logger
from scrapper.components.mongo import Mongo


@newrelic.agent.background_task()
class Scrapper:
    """ Extractor for brseries.com  """

    def __init__(self):
        self.mongo_client = Mongo()
        self.log = Logger.get('Scrapper')
        self.tmdb = TMDB()
        self.trakt = Trakt()

    @newrelic.agent.background_task(name='get_pages_total_pages', group='Scrapper')  # NOQA: E501
    def get_pages_total_pages(self):
        """ Find the total of pages to scrap """
        self.log.info('Getting total of pages in %s' % Settings.BRSERIES_URL)
        r = requests.get(Settings.BRSERIES_URL)
        d = pq(r.text)

        pagination = d('.navigation .page-numbers')
        total_pages = d(pagination[-2]).text()

        if total_pages:
            self.log.info('found %s pages' % total_pages)
            return int(total_pages)
        else:
            return 1

    @newrelic.agent.background_task(name='get_shows_from_page', group='Scrapper')  # NOQA: E501
    def get_shows_from_page(self, page=0):
        """
            Request the list of shows
            @page pagination
            returns list of urls
        """
        self.log.info('Getting shows from page %s' % page)

        url = Settings.BRSERIES_URL
        if page:
            url = '{}/page/{}'.format(url, page)

        r = requests.get(url)
        d = pq(r.text)

        li = d(".esquerda .lista-filmes li")
        urls = []
        li.each(lambda i, e: urls.append(self.get_url_to_show(e)))

        return urls

    @newrelic.agent.background_task(name='get_url_to_show', group='Scrapper')
    def get_url_to_show(self, div):
        """
            Get the link from serie
        """
        d = pq(div)
        link = d('a').attr('href')
        return link

    @newrelic.agent.background_task(name='get_episodes', group='Scrapper')  # NOQA: E501
    def get_episodes(self, url):
        r = requests.get(url)
        d = pq(r.text)

        tabs = d('.content .tabs li')
        seasons = []
        tabs.each(lambda i, e: seasons.append(
            {
                'title': pq(e).find('a').text().replace('ª', ''),
                'id': i,
                'episodes': []
            }
        ))

        episodes = []
        for season in seasons:
            tab = d('.content .tab_container .tab_content').eq(season.get('id'))  # NOQA: E501
            subtitled = d(tab).find('.check_lista').eq(1)
            eps = d(subtitled).find('li a')
            eps.each(lambda i, e: episodes.append(
                {
                    "number": d(e).text().replace('Episódio ', ''),
                    "id": d(e).attr('href').split('=')[-1],
                    "season": season.get('title')
                }
            ))

        return episodes

    @newrelic.agent.background_task(name='discover_urls_to_scrap', group='Scrapper')  # NOQA: E501
    def discover_urls_to_scrap(self):
        shows_url = []
        pages = list(range(self.get_pages_total_pages()))  # make iterable

        for p in pages:
            page = p + 1
            urls = self.get_shows_from_page(page)
            shows_url += urls

        return shows_url

    def get_num_seasons(self, show):
        episodes = show.get('episodes', [])
        episodes_seasons = [ep.get('season') for ep in episodes]

        if len(episodes_seasons) > 0:
            seasons = set(episodes_seasons)
            return len(seasons)

        return 0

    def show_need_update(self, show, new_episodes):
        saved_show = self.mongo_client.find_one(show.get('slug'))

        if saved_show is None:
            return True

        saved_episodes = saved_show.get('episodes')
        has_new_episodes = len(new_episodes) > len(saved_episodes)

        return has_new_episodes

    def scrap_show(self, url):
        """
            Get episodes list from brseries by passing the show URL
            Search show info into Trakt
            Search episodes info into Trakt
            Merge all show data
            Save show into DB
        """
        slug = Trakt.sanatize_slug(url)
        self.log.info('Looking for (%s) %s' % (slug, url))

        episodes_list = self.get_episodes(url)
        show = self.trakt.get_trakt_info(slug)

        if (show is not None):
            if self.show_need_update(show, episodes_list):
                reverse_episodes_list = list(reversed(episodes_list))
                show['images'] = self.tmdb.get_show_image(show.get('tmdb_id'))
                show['episodes'] = self.trakt.get_episodes(show, reverse_episodes_list)  # NOQA: E501
                show['num_seasons'] = self.get_num_seasons(show)

                result = self.mongo_client.save_show(show)
                self.log.info('Saved %s' % result.raw_result)
            else:
                self.log.info('Doesn\'t need update %s' % slug)

    @newrelic.agent.background_task(name="run", group="Scrapper")
    def run(self):
        self.log.info('Start Scrapper')
        urls_to_scrap = self.discover_urls_to_scrap()

        for url in reversed(urls_to_scrap):
            self.scrap_show(url)
